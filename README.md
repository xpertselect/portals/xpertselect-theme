# XpertSelect / Portals / XpertSelect Theme

[gitlab.com/xpertselect/portals/xpertselect-theme](https://gitlab.com/xpertselect/portals/xpertselect-theme)

Provides the [Drupal](https://www.drupal.org/) theme for instances running the XpertSelect Portals suite.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect-portals/xpertselect-theme`](https://packagist.org/packages/xpertselect-portals/xpertselect) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect-portals/xpertselect
```

The Composer package includes all the compiled CSS and JS assets. No post-installation compilation is required.

## Extending the provided `xpertselect` theme

The compiled uses the CSS variables listed below. By providing alternative values for these variables one can easily customize the `xpertselect` Drupal theme.

```css
--xs-pri-400:
--xs-pri-600:
--xs-pri-200:

--xs-sec-400:
--xs-sec-600:
--xs-sec-200:

--xs-thi-400:
--xs-thi-600:
--xs-thi-200:

--primary-button-color:
--primary-button-color-hover:
--xs-sec-text-color:

--xs-font-family:
```
