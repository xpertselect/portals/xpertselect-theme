/* globals jQuery, Drupal, drupalSettings */

(function ($, drupalSettings, Drupal) {
  Drupal.behaviors.select2Config = {
    attach: function () {
      "use strict";

      $(function() {
        const themeSelect = $(".select2-widget[data-is-hierarchical]");
        themeSelect.parent().find(".select2-search__field").on( "keydown", function(e) {
          if ( e.which === 8 && e.target.value.trim() === "") {
            e.preventDefault();
            const selectElement = $(this).closest("div.js-form-item").find("select");
            let values = selectElement.val();
            values.pop();
            selectElement.val(values).trigger("change");
            if (values.length !== 0) {
              e.target.value = " ";
            } else {
              e.target.value = "";
            }
          }
        });

        $(".select2-widget").each(function() {
          let element = $(this);
          let config = element.data("select2-config");

          if (element.attr("multiple") === "multiple") {
            config = initMultipleOptionSelect(config, element);
          } else {
            config = initSingleOptionSelect(config);
          }

          if (element.attr("data-is-hierarchical") === "true") {
            config = initHierarchicalOptionsSelect(config);
          }

          element.select2(config);
        });
      });
    }
  };
  function initMultipleOptionSelect(config, element) {
    config = alterTheUrlToIncludeTheCurrentInput(config);
    config = sortGivenOptionsAlphabetically(config);
    config = alterTheTagCreation(config, element.data("regex"));

    return overrideTheDefaultInputLengthMessages(config);
  }

  function initSingleOptionSelect(config) {
    config.allowClear = false;

    return config;
  }

  function initHierarchicalOptionsSelect(config) {
    config = displayTheLabelAsHTML(config);

    return config;
  }

  function alterTheUrlToIncludeTheCurrentInput(config) {
    // override ajax data send method
    if (config.ajax && config.ajax.urlPlaceholder) {
      config.ajax.url = function (params) {
        return config.ajax.urlPlaceholder.replace("QUERY_PLACEHOLDER", params.term);
      };
    }

    return config;
  }

  function sortGivenOptionsAlphabetically(config) {
    config.sorter = function (data) {
      return data.sort((a, b) => a.text.localeCompare(b.text));
    };

    return config;
  }

  function alterTheTagCreation(config, regex) {
    // override create tag method, otherwise the new tags will be prefixed with "$id:"
    config.createTag = function (params) {
      const term = $.trim(params.term);

      // If a regex is set, validate the current input
      if (term === "" || (regex !== undefined && !new RegExp(regex).test(term))) {
        return null;
      }

      return {
        id: term,
        text: term
      };
    };

    return config;
  }

  function overrideTheDefaultInputLengthMessages(config) {
    const lengthMessage = function() {
      return Drupal.t("Entries should be between @min and @max characters long.", {"@min": config.minimumInputLength, "@max": config.maximumInputLength});
    };

    config.language = {
      inputTooShort: lengthMessage,
      inputTooLong: lengthMessage
    };

    return config;
  }

  function displayTheLabelAsHTML(config) {
    const templateOption = function (result) {
      let suggestionLabel = result.text;

      if (result.label) {
        suggestionLabel = result.label;
      }
      return $(`<span id="${ result.id }">${ suggestionLabel }</span>`);
    };

    config.templateResult = function (result) {
      let element = templateOption(result);

      const amount = element.text().split("—").length - 1;
      return element.addClass(`ps-${ amount * 2 }`);
    }; // template of the option in the list

    config.templateSelection = function (result) {
      let element = templateOption(result);

      setTimeout(() => {
        const selectElement = element.closest(".select2-selection__choice");
        selectElement.prop("title", element.text());
      }, 100);

      return element;
    }; // template of the selected option

    return config;
  }
})(jQuery, drupalSettings, Drupal);
