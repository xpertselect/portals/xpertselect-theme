/* globals Drupal */

(function (Drupal) {
  Drupal.behaviors.submitSearchFormOnEnter = {
    attach: function(context) {
      "use strict";

      const searchFormInput = context.querySelector("#xs-searchable-content-search-form > #edit-query");

      if (null === searchFormInput) {
        return;
      }

      searchFormInput.addEventListener("keydown", function(event) {
        if (event.keyCode === 13) {
          this.form.submit();
          return false;
        }
      });
    }
  };

  Drupal.behaviors.openSearchFormOnClick = {
    attach: function() {
      "use strict";

      const searchContainer = document.getElementById("search-container");
      const navbarOpenSearch = document.getElementById("navbar-open-search");
      const navbarCloseSearch = document.getElementById("navbar-close-search");

      if (!(searchContainer && navbarOpenSearch && navbarCloseSearch)) {
        return;
      }

      navbarOpenSearch.addEventListener("click", function() {
        searchContainer.classList.add("open-search");
      });

      navbarCloseSearch.addEventListener("click", function() {
        searchContainer.classList.remove("open-search");
      });
    }
  };
})(Drupal);
