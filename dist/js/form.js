/* globals jQuery, Drupal, drupalSettings */

(function ($, Drupal, drupalSettings) {
  let maxUploadSize = null;

  Drupal.behaviors.fileUploadErrorMessager = {
    attach: function() {
      "use strict";

      if (!("xpertselect" in drupalSettings && "maxUploadSize" in drupalSettings.xpertselect) || maxUploadSize !== null) {
        return;
      }
      maxUploadSize = drupalSettings.xpertselect.maxUploadSize;

      $(document).ajaxError((event, xhr, settings) => {
        if (xhr.status !== 413) {
          return;
        }

        const element = $(`#edit-${ settings.extraData._triggering_element_name.replaceAll("_", "-") }`);

        if (element.length === 0 || element.parent().find("[type=\"file\"]").length === 0) {
          return;
        }

        const messagesContainer = document.createElement( "div" );
        element.parent().parent().parent().prepend(messagesContainer);
        const messages = new Drupal.Message(messagesContainer);
        messages.add(Drupal.t("An unrecoverable error occurred. The uploaded file likely exceeded the maximum file size (@size) that this server supports.", {"@size": maxUploadSize}), {type: "error"});
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
