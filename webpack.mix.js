const mix = require("laravel-mix");

mix.options({
  clearConsole: false,
  manifest: false,
  processCssUrls: false
});

mix.copy("./node_modules/@fortawesome/fontawesome-free/webfonts", "./dist/fonts");

mix.copy("./node_modules/@fontsource/source-sans-pro/files/source-sans-pro-latin-400-normal.woff", "./dist/fonts");
mix.copy("./node_modules/@fontsource/source-sans-pro/files/source-sans-pro-latin-400-normal.woff2", "./dist/fonts");

mix.copy("./node_modules/select2/dist/js/select2.min.js", "./dist/js");
mix.copy("./node_modules/select2/dist/js/i18n/nl.js", "./dist/js/i18n");
mix.copy("./node_modules/select2/dist/css/select2.min.css", "./dist/css");

mix.copy("./assets/js", "./dist/js");
mix.sass("./assets/scss/app.scss", "./dist/css/xpertselect.css");
