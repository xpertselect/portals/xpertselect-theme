# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

### Fixed

---

## Release 1.6.2

### Added

- Add an icon for `Identification number source system`.

---

## Release 1.6.1

### Added

- Add icons for `Type of action` and `Date of action`.

### Fixed

- Resolved a problem where the overrides in `select2-overrides.js` did not initialize properly in Drupal 10.

---

## Release 1.6.0

### Added

- Added a `select2` overrides javascript file.

---

## Release 1.5.2

### Changed

- Make the success button less bright.

---

## Release 1.5.1

### Fixed

- When uploading a file in a form that is larger than allowed, always notify the user.

---

## Release 1.5.0

### Added

- Added a template for rendering HTTP 4xx error pages.
- Translation added in admin panel theme settings

---

## Release 1.4.1

### Fixed

- When a breadcrumb item is too long, truncate it with '…' instead of '&hellip;'.

---

## Release 1.4.0

### Added

- Add icon for 'part of' component
- Add icon for 'publication date' component

### Changed

- NPM critical packages updated
- Extra label 'dossier' added for 'woo_dossier'

---

## Release 1.3.1

### Fixed

- Add a title to each of the footer menus.

---

## Release 1.3.0

### Added

- Allows the addition of two menus to the footer, which will be displayed when the menus have one or more links.

---

## Release 1.2.0

### Added

- Add icon for information category.

---

## Release 1.1.0

### Added

- Add icon for document type.

---

## Release 1.0.0

### Changed

- Upgraded the php version to 8.2.

---

## Release 0.7.2

### Changed

- Give the results details more space on the search result page to reduce the amount of empty space.
- Reduce the hide delay for tooltips.
- Alter the color of the search suggestion icons to match the primary color of the theme.

---

## Release 0.7.1

### Changed

- Alter the styling of the search suggestion to correctly align the icons.

---

## Release 0.7.0

### Changed

- Allow an admin to set the alt text of the logo and the homepage image.
- When a user uses their keyboard to navigate the website, show the skip link on focus.
- Change the `facet collapse` markup from a div to a button.
- Add `aria-expanded` attribute to the `collapse-facets` button.
- Allow users to select the text of a tooltip and close them by pressing escape.
- Configuration in theme settings to enable SiteImprove.
- Add missing translations for `Close searchfield` `Open searchfield`.

---

## Release 0.6.4

### Fixed

- Allow an admin to return from the preview of a node page to the form with a button.

---

## Release 0.6.3

### Changed

- Added a border to the hamburger of the menu to make it visible in high contrast mode.
- Make the search javascript listen to the new search form.

---

## Release 0.6.2

### Changed

- Change some of the styling to allow the new `resultsPerThemeBlock`.
- Add icons for `document` and `dossier`.

---

## Release 0.6.1

### Changed

- Added an icon for `woo_dossier` suggestions.
- Renamed the `document` content type to `woo_document`.
- Created styling for `fieldset` component.
- Added an icon for `type` facet.

---

## Release 0.6.0

### Changed

- Deprecated the old `xs_dataset/SearchFormBlock` templates.
- Created templates for the new `xs_searchable_content/SearchFormBlock`.
- Node form pages no longer show revision and author information.

---

## Release 0.5.4

### Fixed

- Altered some form display styling.

---

## Release 0.5.3

### Fixed

- The script for collapsing the facets will no longer be included in all pages, but only on the necessary pages.

---

## Release 0.5.2

### Fixed

- The titles of the breadcrumb items are now treated as user input and will no longer include rendered HTML.

---

## Release 0.5.1

### Fixed

- The facet displayed on the search result page can now be expanded again on mobile.

---

## Release 0.5.0

### Changed

- The maximum amount of facet values displayed on the search result page is now configurable via the drupalSettings object.

---

## Release 0.4.5

### Fixed

- The search form javascript no longer triggers when no search form is rendered.
- The facet list no longer has a minimum height on mobile viewports.

---

## Release 0.4.4

### Fixed

- The facet list no longer sticks to the bottom of the page when the facet list is smaller than the screen height. 

---

## Release 0.4.3

### Changed

- Replace the links to download/view a distribution on the details page to buttons.

### Fixed

- Use the correct color for the hover state of the third party button on the login page.

---

## Release 0.4.2

### Changed

- Improved the mlt styling on the dataset details page.

---

## Release 0.4.1

### Changed

 - `Main menu` to `Main navigation` for mobile menu.

---

## Release 0.4.0

### Added

- Similar datasets on a dataset's page are now styled.

---

## Release 0.3.0

### Added

- Introduced styling scoped on `.memberships-block` to tune the display of the memberships of a user. This new styling matches the `details` sections used in various forms.

### Fixed

- Add padding to the autocomplete input so the value does not run underneath the search icon.

---

## Release 0.2.2

### Added

- A translation for the aria-label for main menu on smaller viewports.

### Changed

- Updated the CSS selector for the 'Update frequency' element to match the new labels set by `drupal/xs_dataset`.
- The aria-label of the main menu on mobile has been changed to `Main menu`.

---

## Release 0.2.1

### Fixed

- The theme cards in the datasets per theme block now have equal horizontal and vertical whitespace.
- The "skip link" at the top of the page now correctly jumps to the main content of the page.
- The button to show/hide the facets on the search-results page on mobile viewports is correctly marked up as a `<button />`.

---

## Release 0.2.0

### Added

- Add a lock icon in front of private dataset suggestions in the search autocomplete.

### Changed

- Changed the css scope of the donl-chip.
- Changed the focus color to an orange color with better contrast.

### Fixed

- Fixed a spelling mistake in a variable name.

---

## Release 0.1.2

### Fixed

- Fixed an issue where the image in the footer did not have the correct path.

---

## Release 0.1.1

### Changed

- Javascript files are now published in the `dist/js` directory for use.

### Fixed

- The `tests/` directory is no longer included in the published Composer package.
- Updated several references to compiled assets to use the correct path in the `dist/` directory.

---

## Release 0.1.0

### Added

- Ported the `xpertselect` theme from [gitlab.textinfo.nl](https://gitlab.textinfo.nl).
