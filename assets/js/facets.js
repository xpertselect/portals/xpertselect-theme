/* globals jQuery, Drupal, drupalSettings */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.collapseFacets = {
    attach: function() {
      "use strict";

      $("#collapse-facets").on("click", function() {
        $(".facets__facets").toggleClass("d-none");
        $(this).find("i").toggleClass("fa-circle-plus fa-circle-minus");
        $(this).attr("aria-expanded", function(index, attr){
          return attr === "false" ? "true" : "false";
        });
      });

      let facetListLength = undefined;

      if (drupalSettings.xpertselect !== undefined) {
        facetListLength = drupalSettings.xpertselect.facetListLength;
      }

      if (facetListLength === undefined) {
        facetListLength = 10;
      }
      facetListLength++;

      if (facetListLength < 2) {
        return;
      }

      const groups = $(`.facets__group:has(li:nth-child(${ facetListLength }))`)
        .append(`<button class="text-center facets__limiter cursor-pointer" aria-label="${ Drupal.t("Show more") }" ` +
          `aria-expanded="false" title="${ Drupal.t("Show more") }" data-bs-toggle="tooltip" ` +
          "data-placement=\"top\" role=\"tooltip\"><i class=\"fa-solid fa-chevron-down\"></i></button>");

      for (let i = 0; i < groups.length; i++) {
        window.addToolTipToElement(groups[i].lastChild);
      }
      const facets__limiter = $(".facets__limiter");

      if (window.innerWidth > 992) {
        facets__limiter.prev().find(`li:nth-child(n+${ facetListLength })`).slideUp("fast");
      }

      facets__limiter.click(function() {
        const clickedButton = $(this);
        clickedButton.toggleClass("facets__unfolded");
        window.closeTooltipOfElement(clickedButton[0]);

        if (clickedButton.hasClass("facets__unfolded")) {
          clickedButton.prev().find(`li:nth-child(n+${ facetListLength })`).slideDown("fast");
          clickedButton.attr("aria-label", Drupal.t("Show less"));
          clickedButton.attr("aria-expanded", "false");
          clickedButton.children().addClass("fa-chevron-up");
          clickedButton.children().removeClass("fa-chevron-down");
          clickedButton.attr("data-bs-original-title", Drupal.t("Show less"));
        } else {
          clickedButton.prev().find(`li:nth-child(n+${ facetListLength })`).slideUp("fast");
          clickedButton.attr("aria-label", Drupal.t("Show more"));
          clickedButton.attr("aria-expanded", "true");
          clickedButton.children().addClass("fa-chevron-down");
          clickedButton.children().removeClass("fa-chevron-up");
          clickedButton.attr("data-bs-original-title", Drupal.t("Show more"));
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
