/* globals jQuery, Drupal, drupalSettings */
/* @deprecated */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.searchFormAutocomplete = {
    attach: function () {
      if (!("xs_dataset" in drupalSettings)) {
        return;
      }

      const xs_dataset = drupalSettings.xs_dataset;

      if (!("autocomplete" in xs_dataset
        && "url" in xs_dataset.autocomplete
        && "dataset" in xs_dataset.autocomplete.url)) {
        return;
      }

      if (!("search" in xs_dataset
        && "url" in xs_dataset.search)) {
        return;
      }

      const search_path = xs_dataset.search.url;
      let request = null;

      const search_input = $(".autocomplete__form input");

      $(document).mouseup(function(event) {
        const inner_search_container = $("#inner-search-container");

        if (!inner_search_container.is(event.target) && inner_search_container.has(event.target).length === 0) {
          hideSuggestions();
        }
      });

      search_input.on("keyup focus", (event) => {
        if (event.keyCode === 27) {
          search_input.val("");
          hideSuggestions();

          return;
        }

        // Arrow keys and return key
        const navigation_keys = [13, 37, 38, 39, 40];

        if (navigation_keys.includes(event.keyCode)) {
          handleNavigationKeys(event.keyCode);

          return;
        }

        const input = search_input.val();

        if (request !== null) {
          request.abort();
          request = null;
        }

        if (input.length > 1) {
          getAutocompleteResults(search_input.val(), showSuggestions);
        } else {
          hideSuggestions();
        }
      });

      let suggestions = [];

      function getAutocompleteResults(query, callback) {
        const encoded_query = encodeURIComponent(query);
        request = $.ajax({
          url: xs_dataset.autocomplete.url.dataset.replace("QUERY_PLACEHOLDER", encoded_query),
          type: "GET",
          success: (data) => {
            if (data.length === 0) {
              return;
            }

            suggestions = data.map((suggestion) => {
              if (!("title" in suggestion)) {
                return null;
              }

              if (!("url" in suggestion)) {
                return null;
              }

              return {
                title: suggestion.title,
                url: suggestion.url,
                class: "suggestion " + suggestion.visibility,
              };
            }).filter((suggestion) => {
              return suggestion !== null;
            });

            if (suggestions.length > 0) {
              suggestions = [{
                header: Drupal.t("Datasets:")
              }].concat(suggestions);
            }
          },
          complete: () => {
            let suggestionsList = [{
              title: query,
              url: search_path.replace("QUERY_PLACEHOLDER", encoded_query),
              class: "search",
            }].concat(suggestions);

            callback(suggestionsList);
          }
        });
      }
    }
  };

  function handleNavigationKeys(key_code) {
    if (key_code === 13) {
      const selected_url = $(".autocomplete__link.selected a");

      if (selected_url.length === 0) {
        return;
      }

      window.location.href = selected_url.attr("href");
    }

    if (key_code === 40) {
      let selected_current = $(".autocomplete__link.selected");

      if (selected_current.length === 0) {
        selected_current = $("#autocomplete__results .autocomplete__link:first");
        selected_current.addClass("selected");

        return;
      }

      let selected_next = selected_current.next("li");

      if(selected_next.length === 0) {
        selected_next = $("#autocomplete__results .autocomplete__link:first");
      }

      if (!selected_next.hasClass("autocomplete__link")) {
        selected_next = selected_next.next("li");
      }

      selected_current.removeClass("selected");
      selected_next.addClass("selected");

      return;
    }

    if (key_code === 38) {
      let selected_current = $(".autocomplete__link.selected");

      if (selected_current.length === 0) {
        selected_current = $("#autocomplete__results .autocomplete__link:last");
        selected_current.addClass("selected");

        return;
      }

      let selected_prev = selected_current.prev("li");

      if(selected_prev.length === 0) {
        selected_prev = $("#autocomplete__results .autocomplete__link:last");
      }

      if (!selected_prev.hasClass("autocomplete__link")) {
        selected_prev = selected_prev.prev("li");
      }

      selected_current.removeClass("selected");
      selected_prev.addClass("selected");
    }
  }

  function hideSuggestions() {
    const autocomplete_result = $("#autocomplete__results__container");
    autocomplete_result.empty();
    $(".autocomplete__form input").attr("aria-expanded", "false");
  }

  function showSuggestions(suggestions) {
    const autocomplete_result_container = $("#autocomplete__results__container");
    const autocomplete_result = $("<ul></ul>")
      .attr("id", "autocomplete__results")
      .attr("role", "listbox")
      .attr("aria-label", Drupal.t("Search results"));
    $(".autocomplete__form input").attr("aria-expanded", "true");

    suggestions.forEach((suggestion) => {
      if ("header" in suggestion) {
        autocomplete_result.append($("<li></li>").append($("<h5></h5>").html(suggestion.header)));

        return;
      }

      if ("title" in suggestion && "url" in suggestion) {
        autocomplete_result.append(
          $(`<li class="autocomplete__link ${suggestion.class}" role="option"></li>`)
            .append($(`<a class="autocomplete__link__data" href="${suggestion.url}"></a>`).html(`<span>${suggestion.title}</span>`))
        );
      }
    });

    autocomplete_result_container.html(autocomplete_result);
  }
})(jQuery, Drupal, drupalSettings);
