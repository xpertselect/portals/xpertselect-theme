/* globals Drupal, bootstrap */

(function (Drupal, bootstrap) {
  Drupal.behaviors.bootstrapTooltips = {
    attach: function() {
      "use strict";

      // bootstrap tooltips
      const tooltipElementsList = [].slice.call(document.querySelectorAll("[data-bs-toggle=\"tooltip\"]"));
      const tooltipsList = [];
      tooltipElementsList.forEach(addToolTipToElement);

      // Close tooltip when a user presses esc
      document.addEventListener("keyup", (e) => {
        if (e.key === "Escape") {
          tooltipsList.forEach((tooltip) => {
            tooltip.hide();
          });
        }
      });

      function addToolTipToElement(tooltipElement) {
        const tooltip = new bootstrap.Tooltip(tooltipElement, {
          "delay": { show: 500, hide: 200 },
        });
        // Allow a user to select the text of a tooltip
        tooltipElement.addEventListener("hide.bs.tooltip", (e) => {
          if (document.querySelector("#" + tooltip.tip.id + ":hover") !== null) {
            e.preventDefault();

            tooltip.tip.addEventListener("mouseout", function tooltipHoverExit() {
              if (document.querySelector("[data-bs-toggle=\"tooltip\"]:hover") !== tooltipElement) {
                tooltip.hide();
              }
              tooltip.tip.removeEventListener("mouseout", tooltipHoverExit, false);
            });
          }
        });

        tooltipsList.push(tooltip);
      }

      function closeTooltipOfElement(tooltipElement) {
        const tooltipIndex = Number(tooltipElement.getAttribute("data-tooltip-index"));
        tooltipsList.at(tooltipIndex).hide();
      }

      window.addToolTipToElement = addToolTipToElement;
      window.closeTooltipOfElement = closeTooltipOfElement;
    }
  };
})(Drupal, bootstrap);
