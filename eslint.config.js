const eslint = require("@eslint/js");
const globals = require("globals");

/** @type {import('eslint').Linter.FlatConfig[]} */
module.exports = [
  {
    languageOptions: {
      ecmaVersion: 6,
      sourceType: "module",
      globals: Object.assign(globals.es2015, globals.node, globals.browser)
    },
    rules: Object.assign({
      "indent": ["error", 2],
      "linebreak-style": ["error", "unix"],
      "quotes": ["error", "double"],
      "semi": ["error", "always"],
    }, eslint.configs.recommended.rules),
  },
];
